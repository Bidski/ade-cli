.. _usage:

Usage
-----

The goal of this section is to explain how to use ADE on a project that provides
and ADE configuration. See :ref:`setup` for more information.


ADE Home
^^^^^^^^

ADE needs a directory on the host which will be mounted as the user's
home directory within the container. It will be populated with
dotfiles and must be different than the user's home directory
on the host. In case you use ADE for multiple projects it
is recommended to use dedicated ADE home directories for each project.

ADE will look for a directory containing a file named ``.adehome``
starting with the current working directory and continuing with the
parent directories to identify the ADE home directory to be mounted.

::

   mkdir adehome
   cd adehome
   touch .adehome


Quick start
^^^^^^^^^^^

To start ``ade``, an ``.aderc`` file is needed. For an example,
see the `Autoware.Auto project <https://gitlab.com/AutowareAuto/AutowareAuto>`_. For
instructions to create an ``.aderc`` file for a project, see :ref:`setup`.

When ``ade`` is run for the first time, it will first prompt for an authentication token,
and then download all the Docker images configured in the ``.aderc`` file

::

   $ cd adehome/path/to/.aderc
   $ ade start --update
   ...
   ADE startup completed.
   $ ade enter
   ade$ <= Note that the prompt changes


CLI
^^^

.. click:: ade_cli.cli:ade
   :prog: ade
   :show-nested:
   :commands: start, enter, stop, save, load, update-cli



Environment Variables
^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2
   :hidden:

   addargs
   ade-env-vars

It is also possible to configure the default behavior of ``ade`` using environment variables
and the ``.aderc`` file. For more information, see :ref:`ade-env-vars` and :ref:`aderc-file`.


Cleanup
^^^^^^^

Over time, unused Docker images, containers, and volumes will clutter the machine's hard drive.
ADE does not store anything valuable inside the Docker containers, so it is possible to use
native Docker commands to clean up. Anything valuable that needs to persist should be placed
in ``ade-home``, which is stored on the host and mounted in ADE.

To get an overview of Docker's disk usage, run::

   $ docker system df
   TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
   Images              13                  11                  14.03GB             916.9MB (6%)
   Containers          11                  0                   2.311MB             2.311MB (100%)
   Local Volumes       17                  15                  5.411GB             17.8MB (0%)
   Build Cache         0                   0                   0B                  0B

Docker provides a cleanup command that will remove everything that is unused. To properly
determine what is unused, **make sure that all Docker containers you want to keep are running.**
To avoid having to re-download ADE images, run ``ade start``.
Once you are certain that everything you want to keep is in use, run::

   $ docker system prune -a --volumes


Debugging
^^^^^^^^^

To see the native Docker commands and other commands `ade` is executing, set the ``ECHO`` variable
before running `ade`, e.g. ::

   $ ECHO=1 ade start


.. _start-multiple-ade:

Starting multiple ADE instances
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When working on multiple projects with ADE configurations, it is
desired to keep an instance of ADE running for each project. To run
multiple instance, use the ``ADE_NAME`` environment variable to
switch between the two instances. e.g.

Let's say that there are two project `minimal-ade`_ and `AutowareAuto`_,
cloned in ``ade-home``:

**Note**
  It is recommended to keep
  separate ADE homes for each project; however, nothing prevents using
  the same ADE home for two projects.

Start the ADE instance for *minimal-ade*::

   $ cd ~/ade-home/minimal-ade
   $ export ADE_NAME=minimal
   $ ade start

Then start the ADE instance for *AutowareAuto*::

   $ cd ~/ade-home/AutowareAuto
   $ export ADE_NAME=aw
   $ ade start

Now, in a new terminal, it is possible to select the ADE instance by
setting ``ADE_NAME``::

   $ cd ~/ade-home
   $ export ADE_NAME=minimal
   $ ade enter
   minimal$ # Inside minimal ADE prompt

**Note**
  The hostname in the ADE container will match ``ADE_NAME``, indicating
  the current ADE instance

Similarly, specifc ADE instances can be stopped::

   $ cd ~/ade-home
   $ export ADE_NAME=aw
   $ ade stop
   Stopping aw
   ...


Limitations
"""""""""""

Starting multiple ADE works well when using the default network configuration;
however, ADE instances may conflict if they use custom network configurations:

* If one ADE instance binds to a port (see :ref:`addargs`), a second instance will not be
  able to bind to the same port.
* If one instance uses a Macvlan network (see :ref:`start-ade-macvlan`), a second Macvlan network must
  be created for the second instance.


.. _minimal-ade: https://gitlab.com/ApexAI/minimal-ade
.. _AutowareAuto: https://gitlab.com/AutowareAuto/AutowareAuto
