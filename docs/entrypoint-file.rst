.. _entrypoint-file:

The ``entrypoint`` file
^^^^^^^^^^^^^^^^^^^^^^^

As described in :ref:`create-custom-base-image`, one of the components of an
ADE-compatible base image is the ``entrypoint`` file:

  .. literalinclude:: minimal-ade/entrypoint
     :language: bash

Let's break down the main parts:

1. There are a few lines to help debug the ``entrypoint``:

  .. literalinclude:: minimal-ade/entrypoint
     :language: bash
     :lines: 6-7

  .. literalinclude:: minimal-ade/entrypoint
     :language: bash
     :lines: 13-15

2. The Docker image should behave like a regular Docker image, if it's running in CI:

  .. literalinclude:: minimal-ade/entrypoint
     :language: bash
     :lines: 8-10

  This ensures that ADE and CI can work together to provide developers a single, reproducible
  environment.

3. The user's environment is configured inside ADE:

   Set the timezone

      .. literalinclude:: minimal-ade/entrypoint
         :language: bash
         :lines: 18-22

      This ensures the programs, like ``git commit`` will have the correct time.

   Create a user inside the container and make sure it's in the correct groups

      .. literalinclude:: minimal-ade/entrypoint
         :language: bash
         :lines: 25-41

      This ensures that the user on the host and inside ADE behave the same way. This step
      includes creating a home directory based on ``/etc/skel/``, not unlike a Linux
      distribution creates a home directory

4. It calls the ``.adeinit`` scripts of any volumes:

   .. literalinclude:: minimal-ade/entrypoint
      :language: bash
      :lines: 44-52

   This gives a way for volumes to provide scripts that must be run on startup to configure
   the ADE instance appropriately for the volume. See :ref:`create-custom-volume` also.

5. Finally, it starts ADE:

   .. literalinclude:: minimal-ade/entrypoint
      :language: bash
      :lines: 55-56
